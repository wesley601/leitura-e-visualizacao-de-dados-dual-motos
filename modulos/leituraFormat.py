# -*- coding: UTF-8 -*-
import pandas as pd

caminho = '/home/Sants/Documentos/Projeto pra loja/leitura-e-visualizacao-de-dados-dual-motos/'

leitura = lambda filename : open(filename, encoding='latin-1').readlines()


def concertaMarca():
    df = pd.read_csv(caminho+'inventario2.csv')
    df2 = pd.read_csv(caminho+'rotatividadeModelo.csv')

    """preparando dados"""
    inventario = df.set_index('CODIGOUNID')
    codigos = df2['codigo'].astype('int64')
    rotatividade = df2.set_index('codigo')

    for cod in codigos:
        try:
            inventario.loc[cod]
        except KeyError:
            print(rotatividade.loc[cod])
        else:
            rotatividade.loc[cod, 'Descricao'] = inventario.loc[cod, 'DESCRICAO']

    rotatividade.to_csv(caminho+'rotatividade_formatada.csv')


def trocaVirgulaPonto(value):
    u"""Muda a separação de casas decimais de virgula para ponto
    e tira o ponto das casas de milhar.
    """
    aux1 = value.replace('.', '')
    aux1 = aux1.replace(',', '.')
    aux1 = aux1.strip()

    return aux1


def formatacaoDados(filename):
    '''main
    Lê o arquivo de rotatividade e transforna os dados em um dicionario.
    '''
    arquivo = open(caminho+'arquivos/'+filename, "r")

    produtos = {"codigo": [], "Descricao": [], "Qt_Saida": [],
                "Vl_Total": [], "Estoque": [], "Giro_Med": [],
                "Estoq_min": [], "Sugestao": [], "Ult_Ent": [], "Ult_Sai": []}

    for i in arquivo:

        produtos["codigo"].append(i[0:6].strip())
        produtos["Descricao"].append(i[6:46].strip())
        produtos["Qt_Saida"].append(trocaVirgulaPonto(i[46:59]))
        produtos["Vl_Total"].append(trocaVirgulaPonto(i[54:68]))
        produtos["Estoque"].append(trocaVirgulaPonto(i[68:78]))
        produtos["Giro_Med"].append(trocaVirgulaPonto(i[78:91]))
        produtos["Estoq_min"].append(trocaVirgulaPonto(i[91:102]))
        produtos["Sugestao"].append(trocaVirgulaPonto(i[102:112]))
        produtos["Ult_Ent"].append(i[113:121].strip())
        produtos["Ult_Sai"].append(i[121:135].strip())

    arquivo.close()
    return produtos


def escrita(filename, linhas):

    arquivo = open(filename, "w")

    for linha in linhas:
        if len(linha) < 6:
            continue

        if linha[6].isdigit():
            linha = linha[5:]

            arquivo.write(linha)

    arquivo.close()


def txtToCsv(filename):
    linhas = formatacaoDados(filename)
    # linhas = concertaMarca(linhas)
    newFile = open(filename+'.csv', 'w')

    newFile.write('codigo,Descricao,Qt_Saida,Vl_Total,Estoque,Giro_Med,'
                  'Estoq_min,Sugestao,Ult_Ent,Ult_Sai\n')
    for i in range(len(linhas['Descricao'])):
        # limpando dados
        aux = linhas['Descricao'][i].replace('!', '')
        aux = aux.replace('"', '')
        aux = aux.replace(',', '.')
        # print(linhas['Descricao'][i])
        # termina da limpeza

        prod = f"""{linhas['codigo'][i]},{aux},{linhas['Qt_Saida'][i]},{linhas['Vl_Total'][i]}
        ,{linhas['Estoque'][i]},{linhas['Giro_Med'][i]},{linhas['Estoq_min'][i]},{linhas['Sugestao'][i]}
        ,{linhas['Ult_Ent'][i]},{linhas['Ult_Sai'][i]}"""

        newFile.write(prod+'\n')

    newFile.close()


if __name__ == '__main__':
    concertaMarca()
