# -*- coding: UTF-8 -*-
import matplotlib.pyplot as plt
#from operator import itemgetter

def graflinha(t, barras, titulo="", tleixox="", tleixoy="", grLabel=None):

    barras.sort(key=lambda x: x[2])

    # descrição do grafico
    plt.xticks(rotation=50)
    plt.title(titulo)
    plt.xlabel(tleixox)
    plt.ylabel(tleixoy)
    #plt.figure(figsize=(15,10))
    #plt.style.use('test')
    # plot
    for linha in barras:
        print(f" eixo X {linha[1]}, eixo Y {linha[2]}, label = {linha[0]}")
        #print(linha[2][10:])
        plt.bar(linha[0][t:], linha[2])

    #plt.legend()
    plt.show()
