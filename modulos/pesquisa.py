# -*- coding: UTF-8 -*-
def pesquisa(produtos, valor):
    barras = []

    for i in range(len(produtos["Descricao"])):

        if produtos["Descricao"][i][0:len(valor)] == valor:
            linha = []
            # titulo da barra
            linha.append(produtos["Descricao"][i])

            # eixo x
            # linha.append(produtos["Ult_Ent"][i])
            linha.append(produtos["Ult_Sai"][i])

            # eixo y
            temp = produtos["Vl_Total"][i].replace(".", "")
            temp = temp.replace(",", ".")

            linha.append(float(temp))

            barras.append(linha)
    return barras
