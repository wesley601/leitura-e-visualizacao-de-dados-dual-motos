# -*- coding: UTF-8 -*-
from modulos import leituraFormat as lf
from modulos import graficos
from modulos import pesquisa

from os import getcwd


class main():

    def __init__(self):
        # self.oqFazer = oqFazer
        self.produto = {}
        self.pasta = getcwd()+"/arquivos/"
        self.filename = ''
        self.newFilename = ''

    # caminho = input('Digite o arquivo')
    # nvArquivo += input('novo arquivo')

    def limpezaSimples(self, filename, newFilename):
        self.filename = filename
        self.newFilename = newFilename
        self.pasta += self.filename
        self.produto = lf.leitura(self.pasta)
        lf.escrita(newFilename, self.produto)

    def pesquisaDados(self, pesq, filename=None):
        if filename:
            self.produto = lf.formatacaoDados(self.newFilename)
        else:
            self.produto = lf.formatacaoDados(filename)

        barras = pesquisa.pesquisa(self.produto, pesq)
        graficos.graflinha(len(pesq), barras, "teste", '', '')
        print(barras)

    def converteCsv(self, filename=None):
        lf.txtToCsv(filename)


if __name__ == '__main__':
    main = main()
    # main.converteCsv('arquivos/rotatividadeModelo')
    oqFazer = int(input('O que vamos fazer?\nLimpeza simples de dados = 1\n'
                        'Pesquisa de dados = 2\nConverter para csv = 3'))

    if oqFazer == 1:
        filename = input('nome do arquivo a ser lido')
        newFilename = input('nome do novo arquivo')
        main.limpezaSimples(filename, newFilename)

    elif oqFazer == 2:
        if main.newFilename:
            vlpesq = input('digite o valor a ser pesquisado')
            main.pesquisaDados(vlpesq)
        else:
            main.newFilename = input('digite o nome do arquivo')
            vlpesq = input('digite o valor a ser pesquisado')
            main.pesquisaDados(vlpesq, main.newFilename)
    elif oqFazer == 3:
        main.converteCsv()
# produto = lf.leitura("outroArk")

# lf.escrita("1trimeste", produto)

# produtos = lf.formatacaoDados("novaNovaRotatividade")

# valor = input('digite o produto a ser pesquisado')

# barras = pesquisa.pesquisa(produtos, valor)

# graficos.graflinha(barras,"teste",'','')
