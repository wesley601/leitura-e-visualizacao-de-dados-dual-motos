"""Script para automatizar o processo de limpeza e
   analise da tabela de rotatividade."""

import pandas as pd
# from modulos.leituraFormat import txtToCsv

def trocaVirgulaPonto(value):
    u"""Muda a separação de casas decimais de virgula para ponto
    e tira o ponto das casas de milhar.
    """
    aux1 = value.replace('.', '')
    aux1 = aux1.replace(',', '.')
    aux1 = aux1.strip()

    return aux1


def formatacaoDados(filename):
    '''main
    Lê o arquivo de rotatividade e transforna os dados em um dicionario.
    '''
    arquivo = open('arquivos/'+filename, "r")

    produtos = {"codigo": [], "Descricao": [], "Pedidos": [], "Estoque": []}

    for i in arquivo:

        produtos["codigo"].append(i[0:6].strip())
        produtos["Descricao"].append(i[6:46].strip())
        produtos["Pedidos"].append(trocaVirgulaPonto(i[47:59]))
        produtos["Estoque"].append(trocaVirgulaPonto(i[125:135]))

    arquivo.close()
    return produtos


def txtToCsv(filename):
    linhas = formatacaoDados(filename)
    # linhas = concertaMarca(linhas)
    newFile = open(filename+'.csv', 'w')

    newFile.write('codigo,Descricao,Pedidos,Estoque\n')
    for i in range(len(linhas['Descricao'])):
        # limpando dados
        aux = linhas['Descricao'][i].replace('!', '')
        aux = aux.replace('"', '')
        aux = aux.replace(',', '.')
        # print(linhas['Descricao'][i])
        # termina da limpeza

        prod = f"""{linhas['codigo'][i]},{aux},{linhas['Pedidos'][i]},{linhas['Estoque'][i]}"""

        newFile.write(prod+'\n')

    newFile.close()


def funcao1():
    with open('test.txt', encoding='latin-1') as file:
        data = file.readlines()

    data2 = []

    for line in data:
        if line[0].isdigit():
            data2.append(line)

def colunaSN(filename):
    df = pd.read_csv(filename)
    coluna = []
    for b in range(len(df['Pedidos'])):
        if df['Pedidos'][b] <= df['Estoque'][b]:
            coluna.append("sim")
        else:
            coluna.append("não")

    df['condicionais'] = coluna

    df.to_csv('teste3.csv')

# with open('tt.txt', 'w') as fi:
#     for l in data2:
#         fi.write(l)

colunaSN('teste1.csv')
