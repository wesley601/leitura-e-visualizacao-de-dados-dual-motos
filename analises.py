from modulos.leituraFormat import leitura
import pandas as pd
import nltk

def txtToCsv(filename):
    linhas = leitura(filename)
    newFile = open(filename+'.csv','w')

    for ln in linhas:
        aux = ln[12:62].strip().replace('!','')
        aux = aux.replace('"','')

        prod = ln[0:11].strip() +','+ aux +','+ ln[63:73].strip()+'\n'

        newFile.write(prod)

    newFile.close()

df = pd.read_csv('arquivos/rotatividade_formatada.csv')

descricoes = []
marcas = {}

for i, desc in enumerate(df['Descricao']):
    descricoes = df['Descricao'][i].split()

    if descricoes[-1] not in marcas:
        marcas[descricoes[-1]] = 1
    else:
        marcas[descricoes[-1]] += 1

sair = '1'
while sair != 'sair':
    sair = input('o que fazer?')
    if sair == 'mostra tudo':
        print(marcas)
    elif sair == 'busca item':
        marca = input('qual marca?')
        print(marcas.get(marca, 'Não encontrado'))

    elif sair == 'maior':
        marca = max(marcas, key=marcas.get)
        print(f'{marca}:{marcas[marca]}')
